package com.example.juhom.finnkino;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import static android.content.ContentValues.TAG;

public class MainActivity extends Activity {

    private ArrayAdapter dataAdapter;
    private Spinner spinner;
    CinemaList cinemalist = CinemaList.getInstance();
    public ArrayList<Cinema> Cinema_list;
    private TextView mDisplayDate;
    private DatePickerDialog.OnDateSetListener mDateSetListener; // HOX
    private static final String TAG = "MainActivity";
    public ArrayList movie_list = null;
    public String date2;
    public String mID;
    private ListView listView;
    EditText time1 = null;
    EditText time2 = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        time1=(EditText)findViewById(R.id.time1);
        time2=(EditText)findViewById(R.id.time2);

        // PVM valitsin
        //######################################################
        mDisplayDate = (TextView) findViewById(R.id.tvDate);
        listView = (ListView) findViewById(R.id.listView);

        mDisplayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog dialog = new DatePickerDialog(
                        MainActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: mm/dd/yyy: " + month + "/" + day + "/" + year);

                String date = month + "/" + day + "/" + year;
                date2 = day + "." + month + "." + year;
                mDisplayDate.setText(date);
            }
        };

        // ##############################

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        spinner = (Spinner) findViewById(R.id.spinner);

        cinemalist.readXML();

        Cinema_list = cinemalist.getCinemaList();
        dataAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, Cinema_list);
        spinner.setAdapter(dataAdapter);


    }

    public void searchMovies(View v) {
        ArrayAdapter movieAdapter;
        int selection = spinner.getSelectedItemPosition();
        Cinema c = Cinema_list.get(selection);
        mID = c.getID();
        String stringTime1 = "00:00";
        String stringTime2 = "23:59";

        if (time1.length() != 0) {
            stringTime1= time1.getText().toString();
        }
        if (time2.length() != 0)  {

            stringTime2= time2.getText().toString();
        }

        stringTime1 = stringTime1.replace(":", "");
        int intTime1=Integer.parseInt(stringTime1);

        stringTime2 = stringTime2.replace(":", "");
        int intTime2=Integer.parseInt(stringTime2);

        System.out.println(intTime1 + "    "+ intTime2);

        movie_list = cinemalist.getMovie(mID, date2, intTime1, intTime2);
        System.out.println("LISTATAAN");
        System.out.println(movie_list);
        movieAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, movie_list);
        listView.setAdapter(movieAdapter);

    }


    }






