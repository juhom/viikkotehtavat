package com.example.juhom.finnkino;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class CinemaList {

    CinemaList cinemalist = CinemaList.getInstance();

    public ArrayList<Cinema> Cinema_list;
    public ArrayList movie_list;

    private static final CinemaList instance = new CinemaList();

    private CinemaList(){
        Cinema_list = new ArrayList();
        movie_list = new ArrayList();
    }

    public static CinemaList getInstance(){
        return instance;
    }
    public void readXML () {
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            String urlString = "https://www.finnkino.fi/xml/TheatreAreas/";
            Document doc = builder.parse(urlString);
            doc.getDocumentElement().normalize();
            System.out.println("Root element: " + doc.getDocumentElement().getNodeName());



            NodeList nList = doc.getDocumentElement().getElementsByTagName("TheatreArea");

            for (int i = 0; i < nList.getLength(); i++) {
                Node node = nList.item(i);


                if (node.getNodeType() == Node.ELEMENT_NODE) {

                    String cID;
                    String cName;
                    Element element = (Element) node;

                   /* System.out.print("ID:");
                    System.out.println(element.getElementsByTagName("ID").item(0).getTextContent());
                    System.out.print("Name:");
                    System.out.println(element.getElementsByTagName("Name").item(0).getTextContent()); */

                    cID = element.getElementsByTagName("ID").item(0).getTextContent();
                    cName = element.getElementsByTagName("Name").item(0).getTextContent();

                    Cinema_list.add(new Cinema(cID, cName));

                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } finally {
            System.out.println("########DONE#########");
        }
    }

    public ArrayList getMovie(String mID, String date2, int intTime1, int intTime2) {
        movie_list = new ArrayList();

        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

            String urlString = "https://www.finnkino.fi/xml/Schedule/?area=" + mID + "&dt=" + date2;
            Document doc = builder.parse(urlString);
            doc.getDocumentElement().normalize();
            System.out.println("Root element: " + doc.getDocumentElement().getNodeName());


            NodeList nList = doc.getDocumentElement().getElementsByTagName("Show");

            for (int i = 0; i < nList.getLength(); i++) {
                Node node = nList.item(i);


                if (node.getNodeType() == Node.ELEMENT_NODE) {

                    String mName = "";
                    String mTime = "";
                    Element element = (Element) node;

                    mName = element.getElementsByTagName("Title").item(0).getTextContent();
                    mTime = element.getElementsByTagName("dttmShowStart").item(0).getTextContent();

                    // päivämäärän ja ajan muotoilu

                    SimpleDateFormat clock = new SimpleDateFormat("YYYY-MM-dd hh:mm:ss");
                    String s1 = mTime;
                    s1 = s1.replace("T", " ");
                    String s2 = s1;
                    String[] parts = s2.split(" ");
                    String pvm = parts[0]; // pvm
                    String time = parts[1]; // aika
                    String time2 = time;
                    String[] parts2 = time2.split(":");
                    String time3 = parts2[0]+parts2[1]; // tunnit ja minuutit
                    String printTime= parts2[0]+":"+parts2[1];
                    int itime3 = Integer.parseInt(time3);
                    System.out.println(itime3);


                    System.out.println(time3);

                    if (itime3 >= intTime1 && itime3 <= intTime2) {
                        movie_list.add(mName + ", " + pvm + " klo " +printTime);
                        System.out.println(mName + "," + pvm + " " +printTime);
                    }
                    else {
                        System.out.println("Ei elokuvia");
                    }








                }

            }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } finally {
            System.out.println("########DONE#########");
        }

        return movie_list;

    }
   /* public void AddCinema(String cID, String cName) {

        Cinema_list.add(new Cinema(cID, cName));

    }*/


    public ArrayList<Cinema> getCinemaList() {
        return Cinema_list;
    }

}
