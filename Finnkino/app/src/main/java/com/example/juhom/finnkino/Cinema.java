package com.example.juhom.finnkino;

public class Cinema {

    private String id;
    private String name;

    public Cinema(String _id, String _name) {
        id = _id;
        name = _name;

    }

    public String getName() {
        return name;
    }

    public String getID() {
        return id;
    }


    @Override

    public String toString() {
        return (name);
    }


}
