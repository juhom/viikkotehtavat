package com.example.juhom.firstproject;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

    private EditText nimi;
    private TextView tulostus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nimi = (EditText) findViewById(R.id.input);
        tulostus = (TextView) findViewById(R.id.output);

        nimi.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                tulostus.setText(nimi.getText());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void buttonClick(View v) {

        Button button = (Button) v;

    }




}
