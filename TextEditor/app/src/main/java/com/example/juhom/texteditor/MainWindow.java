package com.example.juhom.texteditor;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BlankFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BlankFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainWindow extends Fragment {

    TextView View;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private int Size = 20;
    private int Color1 = Color.BLACK;
    private String text2;
    private int Style = Typeface.NORMAL;
    private String Listener;


    public MainWindow() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BlankFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MainWindow newInstance(String param1, String param2) {
        MainWindow fragment = new MainWindow();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString("textSize");
            Size = Integer.parseInt(mParam1);
            System.out.println("Parametri 1: " + mParam1);
            Color1 = getArguments().getInt("textColor");
            System.out.println("Parametri 2:  " + mParam2);
            //Color1 = Integer.parseInt(mParam2);
            System.out.println("VÄRI   "+Color1);
            text2 = getArguments().getString("text");

            mParam1 = getArguments().getString("textSize");
            Size = Integer.parseInt(mParam1);
            Color1 = getArguments().getInt("textColor");
            Style = getArguments().getInt("textStyle");
            Listener = getArguments().getString("listener");


        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

      /*  String strtext = getArguments().getString("textSize");
        System.out.println("Parametri 2: " + strtext);*/



        return inflater.inflate(R.layout.fragment_blank, container, false);
    }

    @Override
    public void onViewCreated (View view, Bundle savedInstanceState) {

        View=getView().findViewById(R.id.textView2);
       // Edit=getView().findViewById(R.id.editText2);
        View.setText(text2);
        View.setTextSize(Size);
        View.setTextColor(Color1);
        View.setTypeface(null, Style);
        //Edit.SetKeyListener(Listener);


        /*String strtext = getArguments().getString("textSize");
        System.out.println("Parametri 3: " + strtext);*/
    }


}
