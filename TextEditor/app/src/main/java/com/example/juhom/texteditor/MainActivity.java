package com.example.juhom.texteditor;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.KeyListener;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    EditText Edit;
    TextView View;


    Bundle bundle = new Bundle();

    public int textSize = 20;
    public String textColor = "Color.BLACK";
    public String textStyle = "Typeface.BOLD";
    public String listener = "";
    public String color = "";
    public String text = "JEEE JEEE EJEEE JEEE JEEE JEEE JEEE EJEEEEEEE";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        bundle.putInt("textColor", Color.BLACK);
        bundle.putString("textSize", "20");
        bundle.putString("text", text);
        bundle.putInt("textStyle", Typeface.NORMAL);




        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Edit = (EditText) findViewById(R.id.editText2);
        View = (TextView) findViewById(R.id.textView2);

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.Fontti) {
            Font nextFrag = new Font();
            MainWindow nextFrag2 = new MainWindow();
            nextFrag2.setArguments(bundle);

            MainActivity.this.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, nextFrag)
                    .addToBackStack(null)
                    .commit();

        } else if (id == R.id.Väri) {

            MainWindow nextFrag2 = new MainWindow();
            nextFrag2.setArguments(bundle);
            MainActivity.this.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, nextFrag2)
                    .addToBackStack(null)
                    .commit();

        } else if (id == R.id.asettelu) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void Editor(View v) {


        Edit = (EditText) findViewById(R.id.editText2);
        View = (TextView) findViewById(R.id.textView2);

        System.out.println("EDIT TEXT TAAS" + Edit);
        text = Edit.getText().toString();
        View.setText(text);
        Edit.setText("");
        bundle.putString("text", text);

    }

    // ###########  TEXT SIZE  #####################

    public void SizeSmall(View v) {
        textSize = 10;
        bundle.putString("textSize", "10");

    }

    public void SizeMedium(View v) {
        textSize = 20;
        bundle.putString("textSize", "20");
    }

    public void SizeBig(View v) {
        textSize = 30;
        bundle.putString("textSize", "30");
    }
    // #################################################

    // ################# TEXT COLOR  ###################

    public void colorBlue(View v) {
        textColor = "Color.BLUE";
        bundle.putInt("textColor", Color.BLUE);

        //View.setTextColor(Color.BLUE);
    }

    public void colorBlack(View v) {
        textColor = "Color.BLACK";
        bundle.putInt("textColor", Color.BLACK);

    }

    public void colorRed(View v) {
        textColor = "Color.RED";
        bundle.putInt("textColor", Color.RED);

    }

    // ####################################################

    // #################### TEXT SPACING #############################

    public void smallLine(View v) {
        View.setLineSpacing(1, 2);
    }

    public void normalLine(View v) {
        View.setLineSpacing(2, 3);
    }

    public void biglLine(View v) {
        View.setLineSpacing(3, 4);
    }

    // ###################################################################

    // ################### TEXT STYLE ####################################

    public void boldText(View v) {
        bundle.putInt("textStyle", Typeface.BOLD);
    }

    public void normalText(View v) {
        bundle.putInt("textStyle", Typeface.NORMAL);
    }

    public void italicText(View v) {
        bundle.putInt("textStyle", Typeface.ITALIC);
    }

    public void lockEditor(View v) {
        listener = null;
       // bundle.putInt("listener", "null");
    }

    public void unlockEditor(View v) {
       // listener = ???;
       // bundle.putInt("listener", "???");

    }
}