package com.example.juhom.pullokone;

public class Bottle {
        private String name;
        private double size;
        private double price;

        public Bottle(String _name, double _size, double _price) {
            name = _name;
            size = _size;
            price = _price;
        }

        public String getName() {
            return name;
        }

        public double getSize() {
            return size;
        }

        public double getPrice() {
            return price;
        }

        @Override

        public String toString() {
            return (name + " " + size + " " + price + "€");
        }
}

