package com.example.juhom.pullokone;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;


public class MainActivity extends Activity {
    public ArrayList<Bottle> bottle_list;
    private Spinner spinner1;
    BottleDispenser bottledispenser = BottleDispenser.getInstance();
    private ArrayAdapter dataAdapter;
    private TextView print;
    public double value = 0;
    private SeekBar seekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        print=(TextView)findViewById(R.id.output);
        spinner1 = (Spinner) findViewById(R.id.spinner1);

        bottle_list = bottledispenser.getBottleList();
        dataAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, bottle_list);
        //  dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(dataAdapter);

        seekBar = (SeekBar)findViewById(R.id.seekBar);
        final TextView summa = (TextView)findViewById(R.id.summa);


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                value = progress;
                value = 0.5*value;
                summa.setText(String.valueOf(value + "€ / 4€"));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
            });
    }

    public void printTicket() {

    }

    public void addMoney(View v) {
        print.setText(bottledispenser.addMoney(value));
        seekBar.setProgress(0);
    }

    public void buyBottle(View v) {
        int selection = spinner1.getSelectedItemPosition();
        String string = bottledispenser.buyBottle(selection);
        String[] parts = string.split(",");
        String bName = parts[0]; // Pullon nimi
        String bPrice = parts[1]; // Pullon hinta


        print.setText("KACHUNK! " + bName + " tipahti masiinasta!");

        try {
            FileOutputStream fileout = openFileOutput("kuitti.txt", MODE_PRIVATE);
            OutputStreamWriter ows = new OutputStreamWriter(fileout);

            ows.write(bName + ", " + bPrice + "€");
            ows.close();

            /*String contents = "";

            ows.write(contents);
            ows.close();*/


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void takeMoney(View v) {
        print.setText("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f€" + bottledispenser.returnMoney());

    }


}
