package com.example.juhom.pullokone;

import java.util.ArrayList;


public class BottleDispenser {

    public ArrayList<Bottle> bottle_list;
    public int bottles;
    public double money;
    public double Money;

    private static final BottleDispenser instance = new BottleDispenser();

    private BottleDispenser(){


        bottles = 5;
        money = 0;
        Money = 0;

        bottle_list = new ArrayList();


        bottle_list.add(new Bottle("Pepsi Max", 0.5, 1.8));
        bottle_list.add(new Bottle("Pepsi Max", 1.5, 2.2));
        bottle_list.add(new Bottle("Coca-Cola Zero", 0.5, 2.0));
        bottle_list.add(new Bottle("Coca-Cola Zero", 1.5, 2.5));
        bottle_list.add(new Bottle("Fanta Zero", 0.5, 1.95));
        bottle_list.add(new Bottle("Fanta Zero", 0.5, 1.95));
    }

    public static BottleDispenser getInstance(){
        return instance;
    }

    public String addMoney(double value) {
        money = money + value;
        return ("Klink! Lisää rahaa laitteeseen!");
    }

    public String buyBottle(int selection) {
        Bottle bottle = bottle_list.get(selection - 1);
        if (money >= bottle.getPrice()) {
            money -= bottle.getPrice();
            bottles -= 1;
            bottle_list.remove(selection - 1);
            return (bottle.getName()+ "," + bottle.getPrice());

        } else {
            return ("Syötä rahaa ensin!");
        }
    }

    public double returnMoney() {

        Money = money;
        money = 0;
        return Money;
    }

  /*  public String showBottles() {
        for (int i=0; i<=bottles; i++) {
            Bottle bottle = bottle_list.get(i);
            return(i+1 + ". Nimi: " + bottle.getName() + "\n	Koko: " + bottle.getSize() + "	Hinta: " + bottle.getPrice() + "\n");
        }
    } */
    public ArrayList<Bottle> getBottleList() {
        return bottle_list;
    }
}
