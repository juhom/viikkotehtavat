package com.example.juhom.webview;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends Activity {
    EditText address;
    TextView text;

    WebView web;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        address=(EditText)findViewById(R.id.editText);
        text=(TextView)findViewById(R.id.text);

        web = findViewById(R.id.webView);
        web.setWebViewClient(new WebViewClient(){
            public void onPageFinished(WebView view, String url) {
                text.setText(web.getUrl());
            }
        });
        web.getSettings().setJavaScriptEnabled(true);
    }

    public void forwad(View v) {
        // Go Forward if canGoForward is frue
        if(web.canGoForward()){
            web.goForward();
            address.setText("");
        }
    }

    public void backward(View v) {

        // Going back if canGoBack true
        if(web.canGoBack()){
            web.goBack();
            address.setText("");
        }
    }

    public void search(View v) {
        String address2 = address.getText().toString();
        System.out.println(address2);

        if (address2.equals("index.html") ) {
            web.loadUrl("file:///android_asset/index.html");

        } else {
            web.loadUrl("http://" + address2);
        }

    }

    public void refresh(View v) {
        web.reload();
    }

    public void shoutOut(View v) {
        web.evaluateJavascript("javascript:shoutOut()", null);
    }

    public void initialize(View v) {
        web.evaluateJavascript("javascript:initialize()", null);
    }

}
