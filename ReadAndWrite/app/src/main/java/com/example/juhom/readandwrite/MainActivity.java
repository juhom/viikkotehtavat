package com.example.juhom.readandwrite;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends Activity {
    EditText name;
    EditText textmsg;
    private TextView print;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textmsg=(EditText)findViewById(R.id.editText);
        name=(EditText)findViewById(R.id.editText2);
        print=(TextView)findViewById(R.id.output);
    }

    public void readFile(View v){
        try {

            FileInputStream fileIn = openFileInput(name.getText().toString());
            InputStreamReader inr = new InputStreamReader(fileIn);

            char[] inputBuffer = new char [300];
            String s = "";
            int charRead;

            while ((charRead = inr.read(inputBuffer))>0) {
                String readstring = String.copyValueOf(inputBuffer, 0,charRead);
                s += readstring;
                print.setText(s);
            }
            inr.close();


        } catch (Exception e){
           e.printStackTrace();
        }
    }

    public void writeFile(View v) {
        try {
            FileOutputStream fileout = openFileOutput(name.getText().toString(), MODE_PRIVATE);
            OutputStreamWriter ows = new OutputStreamWriter(fileout);
            ows.write(textmsg.getText().toString());
            ows.close();


            String contents = "";

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
